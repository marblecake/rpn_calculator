#include <errno.h>
#include "error_handler.h"
#include "display_util.h"


void errMsg(void){
	switch(errno){
		case ERANGE:         printErrMsg("value out\n\rof range");    break;
		case EDOM:           printErrMsg("undefined\n\rbehaviour");   break;
		case STACKOVERFLOW:  printErrMsg("stackoverflow");            break;
		case STACKUNDERFLOW: printErrMsg("insufficient\n\roperands"); break;
		default:             printErrMsg("an error\n\roccurred");     break;
	}
	errno = 0;
}

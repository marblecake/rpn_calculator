/** @file  stack.h
 *	@brief Last in - first out storage
 *	
 *	Stores the input values from the keypad. 
 *	Inputs will be stored either as numerical 
 *	values or characters. 
 *	Numerical values are the operands.
 * 	Characters can be mathematical operators (+,-,*,/)
 *	or special characters to invoke stack functions 
 *	'p' -> print the latest stack entry
 *	'f' -> print the whole stack
 *	'c' -> resets the stack
 *	'd' -> duplicates the lastest stack entry
 *	'r' -> reverses the last two stack entrys
 *
 *	@author Markus Blechschmidt
 *	@author Bastian Weiser
 */

#ifndef STACK_H
#define STACK_H
#define STACKSIZE 1000

/** @brief initializes the stack 
 *  @param int pointer to the start of the stack
 *  @return
 */
int stackInit(int*);

/** @brief   adds a new entry to the stack
 *  @param   the int value to be stored
 *  @return  0 -> everthing worked
 *  @return -1 -> stackoverflow 
 */
int push(int);

/** @brief   retrieves a value from the stack and deletes it from the stack
 *  @param   none
 *  @return  >0 -> everthing worked
 *  @return  -1 -> stackunderflow 
 */
int pop(void);

/** @brief   retrieves a value from the stack without deleting it
 *  @param   none
 *  @return  >0 -> everthing worked
 *  @return  -1 -> stackunderflow 
 */
int peek(void);

/** @brief  checks if the stack is empty
 *  @param  none
 *  @return 0 -> stack not empty
 *  @return 1 -> stack is empty 
 */
int empty(void);

/** @brief  checks if the stack is full
 *  @param  none
 *  @return 0 -> stack is not full
 *  @return 1 -> stack is full
 */
int full(void);

/** @brief  resets the stack
 *  @param  none
 *  @return 0 - stack should be empty now
 */
int clear(void);

/** @brief  duplicates the last two stack entries
 *  @param  none
 *  @return  0 -> everthing worked
 *  @return -1 -> stackunderflow 
 */
int duplicate(void);

/** @brief  reverses the last two stack entries
 *  @param  none
 *  @return   0 -> everthing worked
 *  @return  -1 -> stackunderflow 
 */
int reverse(void);


char * toString(void);

/** @brief  checks the number of entries on the stack
 *  @param  none
 *  @return int value that represents the number of entries
 */
int getStackSize(void);

/** @brief  concatenates the last stack entry with the numerical value of a character
 *  @param  char to be concatenated
 *  @return   0 -> everthing worked
 *  @return  -1 -> result out of range error
 */
int concat(char);

#endif

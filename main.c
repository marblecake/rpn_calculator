/**
  ******************************************************************************
  * @file       main.c 
  * @brief      Main program body 
  *
  * 
  * @author     Markus Blechschmidt
  * @author  	Bastian Weiser 
  *        	
  *             HAW-Hamburg
  *          	Labor f�r technische Informatik
  *          	Berliner Tor  7
  *          	D-20099 Hamburg
  * @version V1.0
  * @date    2016.04.07
  * 
  *******************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "TI_Lib.h"
#include "tft.h"
#include "keypad.h"
#include "display_util.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stack.h"
#include "calculator.h"
#include "error_handler.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/**
  * @brief  checks whether a char is contained in an array
  * @param  c   - the character to be checked
  * @param  arr - points to the beginning of the char array
  * @return 1   - c is contained in the array
  * @return 0   - c was not found
  */
static int inArray(char c, const char* arr){
	for(int i = 0; i < sizeof(arr); i++){
		if (arr[i] == c)
            return(1);
	}
	return(0);
}

/* Private variables ---------------------------------------------------------*/
static const char MATHOPS[] = {' ','+','-','*','/'};

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  Main program - a reverse polish notation calculator
  * @param  None
  * @return None
  */
	
	
int globStack[STACKSIZE];

int main(void)
{
	int newNum = 1;
	int ret = 0;
	Init_TI_Board();
	stackInit(globStack);
	initDisplay();
	while(1){
		char c = Get_Touch_Pad_Input();
		if(('0'<=c  && c<='9') || inArray(c, MATHOPS) ){	//only print wanted characters
			printInput(c);
		}
		if('0' <= c  && c <= '9'){
			if(newNum){
				ret = push(0);	// begin new number on stack
				newNum = 0;
			}
			ret = concat(c);
		}
		else{
			newNum = 1;
			switch(c){
				case '+': ret = plus();      break;
				case '-': ret = minus();     break;
				case '*': ret = multi();     break;
				case '/': ret = divis();     break;
				case 'p': printTop();        break;
				case 'f': printArray();      break;
				case 'c': clear();           break;
				case 'd': ret = duplicate(); break;
				case 'r': ret = reverse();   break;
				default:break;
			}
		}
		if(ret == -1){
			errMsg();
			ret = 0;
		}
	}
}





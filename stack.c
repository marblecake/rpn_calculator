#include <stdio.h>
#include <errno.h>
#include "stack.h"
#include "error_handler.h"

int* stackPointer;
int pos = 0;

int stackInit(int* sp){
	stackPointer = sp;
	return(stackPointer[0]);
}

int push(int val){
	if( pos < STACKSIZE){
		stackPointer[pos] = val;
		pos++;
		return(0);
	}
	errno = STACKOVERFLOW;
	return(-1);
}

int pop(){
	if( pos > 0){
		pos--;
		return(stackPointer[pos]);
	}
	errno = STACKUNDERFLOW;
	return(-1);
}

int peek(){
	if( pos > 0){
		return(stackPointer[pos-1]);
	}
	errno = STACKUNDERFLOW;
	return(-1);
}

int empty(){
	if( pos == 0){
		return(1);
	}
	return(0);
}

int full(){
	if( pos == STACKSIZE){
		return(1);
	}
	return(0);
}

int clear(){
	pos = 0;
	return(0);
}

int duplicate(){
	if(!empty()){
		push(peek());
		return(0);
	}
	else{
		errno = STACKUNDERFLOW;
		return(-1);
	}
}

int reverse(){
	if(pos >= 2){
		int one = pop();
		int two = pop();
		push(one);
		push(two);
		return(0);
	}
	else{
		errno = STACKUNDERFLOW;
		return(-1);
	}
}

int getStackSize(){
	return(pos);
}

int concat(char c){
	int temp = pop();
	int val = temp*10 + c-'0';
	if(val < 0){
		push(temp);
		errno = ERANGE;
		return(-1);
	}
	push(val);
	return(0);
}

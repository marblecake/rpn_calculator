/** @file  calculator.h
 *  @brief carries out the mathematical operations
 *	
 *  Retrieves the last two entries from the stack and
 *  executes the selected mathematical operation.
 *  Possible operations are addition, subtraction, multiplication and division.
 *  The result will be stored on the stack. 
 *
 *  @author Markus Blechschmidt
 *  @author Bastian Weiser
 */

#ifndef CALCULATOR_H
#define CALCULATOR_H

/** @brief   add the last two stack entries and stores the result on the stack
 *  @param   none
 *  @return  0 -> everthing worked
 *  @return -1 -> possible stackunderflow or result is out of range
 */
int plus(void);

/** @brief   subtracts the last stack entry from the second to last entry
 *  @param   none
 *  @return  0 -> everthing worked
 *  @return -1 -> possible stackunderflow or result is out of range
 */
int minus(void);

/** @brief   multiplies the last two stack entries 
 *  @param   none
 *  @return  0 -> everthing worked
 *  @return -1 -> possible stackunderflow or result is out of range
 */
int multi(void);

/** @brief   divides the second to last entry by the last stack entry - Integer divison
 *  @param   none
 *  @return  0 -> everthing worked
 *  @return -1 -> possible stackunderflow or not allowed mathematical operation (Division by zero)
 */
int divis(void);

#endif

#include <errno.h>
#include "calculator.h"
#include "stack.h"
#include "error_handler.h"



static int getArgs(int* a, int* b){
	if(!empty()){*b = pop();}else{
		errno = STACKUNDERFLOW;
		return(-1);
	}
	if(!empty()){*a = pop();}else{
		push(*b);
		errno = STACKUNDERFLOW;
		return(-1);
	}
	return(0);
}

int plus(){
	int a, b;
	if(getArgs(&a, &b) == -1){return(-1);}
	
	int result = a+b;
	if( ((a>0 && b>0) && result < 0) || ((a<0 && b<0) && result < 0) ){
		errno = ERANGE;
		return(-1);
	}
	push(result);
	return(0);
}

int minus(){
	int a, b;
	if(getArgs(&a, &b) == -1){return(-1);}
	
	int result = a-b;
	if( ((a>0 && b<0) && result < 0) || ((a<0 && b>0) && result < 0) ){
		errno = ERANGE;
		return(-1);
	}
	push(result);
	return(0);
}

int multi(){
	int a, b;
	if(getArgs(&a, &b) == -1){return(-1);}
	
	int result = a*b;
	if (a != 0 && result/a != b) {
		errno = ERANGE;
		return(-1);
	}
	push(result);
	return(0);
}

int divis(){
	int a, b;
	if(getArgs(&a, &b) == -1){return(-1);}
	
	if (b == 0) {
		errno = EDOM;
		return(-1);
	}
	int result = a/b;
	push(result);
	return(0);
}

#include "display_util.h"
#include "TI_Lib.h"
#include "keypad.h"
#include "tft.h"
#include "stack.h"

static int cleared = 0;

void clearDisplay(){
	TFT_set_window(5, 2+OUTPUT_WIDTH+2, 2, OUTPUT_WIDTH, OUTPUT_HEIGHT);
	TFT_set_font_color(YELLOW);
	TFT_set_window(BIGZIF50, 2, 2, OUTPUT_WIDTH, OUTPUT_HEIGHT);
	cleared = 1;

}

int initDisplay(){
	Make_Touch_Pad();
	TFT_cursor_off();
	clearDisplay();
	return(0);
}

static void lineBreak(){
	TFT_newline();
	TFT_carriage_return();
}

void printInput(char input){
	if(!cleared){
	  clearDisplay();
	}
	TFT_putc(input);
}

void printArray(){
	clearDisplay();
	if(!empty()){
		int line = 0;
		char buffer[40];
		int temp[STACKSIZE];
		while(line < OUTPUT_HEIGHT-1 && !empty()){
			temp[line] = pop();
			sprintf(buffer,"%d", temp[line]);
			TFT_puts(buffer);
			lineBreak();
			line++;
		}
		if(!empty()){
			temp[line] = pop();
			sprintf(buffer,"%d", temp[line]);
			TFT_puts(buffer);
			if(!empty()){
				TFT_puts(" ...");
			}
			line++;
		}
		for( int i = line-1; i >=0; i--){
			push(temp[i]);
		}
	}
	else{
		TFT_set_font_color(RED);
		TFT_puts("<empty stack>");
		TFT_set_font_color(YELLOW);
	}
	cleared = 0;
}

void printTop(){
	clearDisplay();
	char buffer[40];
	if(!empty()){
		sprintf(buffer,"%d", peek());
		TFT_puts(buffer);
	}
	else{
		TFT_set_font_color(RED);
		TFT_puts("<empty stack>");
		TFT_set_font_color(YELLOW);
	}
	cleared = 0;
}

void printErrMsg(char* msg){
	clearDisplay();
	TFT_set_window(5, 2+OUTPUT_WIDTH+2, 2, OUTPUT_WIDTH, OUTPUT_HEIGHT);
	TFT_set_font(5);
	lineBreak();
	TFT_set_font_color(RED);
	TFT_puts(msg);
	cleared = 0;
}

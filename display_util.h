/** @file  display_util.h
 *  @brief writes output to the display
 *  
 *  Several functions to either print entries of the stack,
 *  error messages or user input to the display. 
 *
 *  @author Markus Blechschmidt
 *  @author Bastian Weiser
 */

#ifndef DISPLAY_UTIL_H
#define DISPLAY_UTIL_H
#ifndef OUTPUT_WIDTH
#define OUTPUT_WIDTH 20
#endif
#ifndef OUTPUT_HEIGHT
#define OUTPUT_HEIGHT 5
#endif


/** @brief   initializes the display and creates the touchpad
 *  @param   none
 *  @return  0 -> everthing worked as expected
 */
int initDisplay(void);

/** @brief   writes every entry of the stack to the display
 *  @param   none
 *  @return  none
 */
void printArray(void);

/** @brief   prints the last entry of the stack to the display
 *  @param   none
 *  @return  none
 */
void printTop(void);

/** @brief   prints the corresponding error message when an error occured
 *  @param   char pointer to the error message that will be displayed 
 *  @return  none
 */
void printErrMsg(char*);

/** @brief   resets the display to its inital state
 *  @param   none 
 *  @return  none
 */
void clearDisplay(void);

/** @brief   displays the user input on the display
 *  @param   a char that has been entered from the touchpad 
 *  @return  none
 */
void printInput(char);

#endif

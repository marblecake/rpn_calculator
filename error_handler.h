/** @file  error_handler.h
 *  @brief handles errors that occur during runtime 
 *  
 *  Possible errors are:
 *  - value out of range    -> numerical value can not be represented in 32bit
 *  - undefined behaviour   -> division by zero
 *  - stackoverflow         -> stacksize has been exceeded 
 *  - insufficient operands -> not enough operands to perform the operation
 *
 *  @author Markus Blechschmidt
 *  @author Bastian Weiser
 */

#ifndef ERROR_HANDLER_H
#define ERROR_HANDLER_H
#define STACKOVERFLOW 80
#define STACKUNDERFLOW 81

/** @brief   checks whether an error has occured and prints the corresponding error message
 *  @param   none 
 *  @return  none
 */
void errMsg(void);

#endif
